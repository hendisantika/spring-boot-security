package com.hendisantika.springbootsecurity.service;

import com.hendisantika.springbootsecurity.entity.User;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-security-sample
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 08/08/18
 * Time: 07.39
 * To change this template use File | Settings | File Templates.
 */
public interface UserService {
    User findUserByEmail(String email);

    void saveUser(User user);
}